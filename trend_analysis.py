import pandas as pd
import numpy as np
import os
from abc import ABC, abstractmethod
import matplotlib.pyplot as plt
import matplotlib.cm as cm

class SMART_Test_Analyzer(ABC):
    @abstractmethod
    def __init__(self, smart_tests_file_path: str):
        pass

    @abstractmethod
    def _load_smart_tests(self):
        pass

    @abstractmethod
    def plot_smart_data(self, x, y, normalize=False):
        pass

class SMART_Test_Analyzer_Text_Files(SMART_Test_Analyzer):
    def __init__(self, smart_tests_file_path: str):
        super().__init__(smart_tests_file_path)
        self.columns = [
            'ID#', 'ATTRIBUTE_NAME', 'RAW_VALUE'
        ]
        self.smart_tests_file_path = smart_tests_file_path 
        assert os.path.exists(self.smart_tests_file_path), "File does not exist"

        self.file_names = []
        self.smart_test_data = []

        try:
            self._load_smart_tests()

        except Exception as e:
            print(f"An error occurred: {e}")

    def convert_text_files_to_csv(self):
        """
            Convert all text files to csv files
        """
        try:
            for idx, data in enumerate(self.smart_test_data):
                path = os.path.join(self.smart_tests_file_path, f"{self.file_names[idx]}.csv")
                data.to_csv(path, index=False)

                print(f"File {self.file_names[idx]}.csv was created successfully.")

        except Exception as e:
            print(f"An error occurred: {e}")
    
    def plot_smart_data(self, x, y, normalize=False):
        # Placeholder for a plotting function, to be implemented later
        pass

    def _load_smart_tests(self):
        """
            Browse directory for all text files and load them into a pandas DataFrame 
        """
        try:
            # Get all files in the directory
            files = [os.path.join(self.smart_tests_file_path, file) for file in os.listdir(self.smart_tests_file_path) if file.endswith(".txt")]

            for file in files:
                # save file name for later use
                self.file_names.append(file.split("\\")[-1].split(".")[0])

                # Load data from text file using regex separator for sequences of more than one space
                df = pd.read_csv(file, sep=r'\s{2,}', engine='python', header=None, names=self.columns)

                self.smart_test_data.append(df)
            
            if not self.smart_test_data:
                raise ValueError("No data was loaded from the text files")

        except Exception as e:
            print(f"An error occurred: {e}")


class SMART_Test_Analyzer_CSV_Files(SMART_Test_Analyzer):
    def __init__(self, smart_tests_file_path: str):
        super().__init__(smart_tests_file_path)
        self.columns = [
            'ID#', 'ATTRIBUTE_NAME', 'RAW_VALUE'
        ]
        self.smart_tests_file_path = smart_tests_file_path 
        assert os.path.exists(self.smart_tests_file_path), "File does not exist"

        self.smart_test_data = []

        try:
            self._load_smart_tests()

        except Exception as e:
            print(f"An error occurred: {e}")
            raise e


    def plot_smart_data(self, x_attribute, y_attributes, normalize=False, visualize=False, save_to_filepath=None):
        try:
            was_normalized = "Normalized" if normalize else "Raw"

            if not self.smart_test_data:
                print("No data available to plot.")
                return

            # Set up the plot
            fig, ax = plt.subplots()

            # Collect data for each attribute 
            attribute_data = self._collect_data(self.smart_test_data, x_attribute,y_attributes, normalize) 
            
            # Sort data by x-values and plot
            for attr, data in attribute_data.items():
                sorted_x, sorted_y = self._sort(data)
                
                # Plot the sorted data
                color = cm.tab20(np.random.rand())
                ax.plot(sorted_x, sorted_y, '-o', label=attr, color=color)

            ax.set_xlabel(x_attribute)
            ax.set_ylabel(f"{was_normalized} Values")
            ax.set_title("SMART Data Trends Over Time")
            ax.legend(title="Attributes", loc='upper left', bbox_to_anchor=(1, 1))

            if save_to_filepath:
                plt.savefig(save_to_filepath, bbox_inches='tight')

            if visualize:
                plt.show()

        except Exception as e:
            print(f"An error occurred during plotting: {e}")
            raise e

    def _sort(self, data: dict):
        # Convert lists to numpy arrays for easier handling
        x_array = np.array(data['x'])
        y_array = np.array(data['y'])

        # Sort by x_array
        sorted_indices = np.argsort(x_array)
        sorted_x = x_array[sorted_indices]
        sorted_y = y_array[sorted_indices]

        return sorted_x, sorted_y


    def _collect_data(self, df_list:list, x_attribute:str, y_attributes:list, normalize:bool):
        try:

            attribute_data = {attr: {'x': [], 'y': []} for attr in y_attributes if attr != x_attribute}

            for df in df_list:
                # Preserve the x-attribute data
                x_df = df[df['ATTRIBUTE_NAME'] == x_attribute]
                x_values = x_df['RAW_VALUE'].values
                if len(x_values) == 0:
                    print("No data for x attribute:", x_attribute)
                    continue
                x_value = x_values[0]

                # Normalize the data if needed
                if normalize:
                    # Normalize without affecting the x_attribute
                    df_without_x = df[df['ATTRIBUTE_NAME'] != x_attribute]
                    self._normalize_data([df_without_x])
                    df = pd.concat([df_without_x, x_df])

                # Collect data for each attribute
                for y_attr in y_attributes:
                    if y_attr != x_attribute:  # Ensure you are not plotting X as Y
                        y_values = df[df['ATTRIBUTE_NAME'] == y_attr]['RAW_VALUE'].values
                        attribute_data[y_attr]['x'].extend([x_value] * len(y_values))
                        attribute_data[y_attr]['y'].extend(y_values)

            return attribute_data

        except Exception as e:
            print(f"An error occurred during data collection: {e}")
            raise e

    def _load_smart_tests(self):
            try:
                files = [os.path.join(self.smart_tests_file_path, file) for file in os.listdir(self.smart_tests_file_path) if file.endswith(".csv")]
                for index, file in enumerate(files):
                    df = pd.read_csv(file, sep=",", names=self.columns, skiprows=1)
                    df['FileIndex'] = index  # Add 'FileIndex' to each DataFrame
                    self.smart_test_data.append(df)

            except Exception as e:
                print(f"An error occurred during file processing: {e}")
                raise e
    

    def _normalize_data(self, df_list: list):
        """
        Normalize the data using z-score normalization.
        """
        try:
            for df in df_list:
                mean_val = df['RAW_VALUE'].mean()
                std_val = df['RAW_VALUE'].std()
                if std_val > 0:  # To avoid division by zero
                    df.loc[:, 'RAW_VALUE'] = (df['RAW_VALUE'] - mean_val) / std_val
                else:
                    df.loc[:, 'RAW_VALUE'] = 0  # In case of no variation
            return df_list

        except Exception as e:
            print(f"An error occurred during normalization: {e}")
            raise e


if __name__ == "__main__":
    try:
        import argparse
        # Get argument from command line to convert or not
        parser = argparse.ArgumentParser(description="Analyze SMART data from text files")

        parser.add_argument("--file_path", type=str, help="Path to the directory containing the text files")
        parser.add_argument("--convert", action="store_true", help="Convert text files to csv files")

        file_path = parser.parse_args().file_path
        convert = parser.parse_args().convert

        if convert:
            # Convert text files to csv files
            test_00 = SMART_Test_Analyzer_Text_Files(file_path)
            test_00.convert_text_files_to_csv()


        else:
            test_00 = SMART_Test_Analyzer_CSV_Files(file_path)

            X = "Power Cycle Count"
            Y_attributes = []

            for att in test_00.smart_test_data[0]['ATTRIBUTE_NAME'].unique():
                if att != X:
                    Y_attributes.append(att)

            test_00.plot_smart_data("Power Cycle Count", 
                                        Y_attributes,
                                        normalize=True, 
                                        visualize=True,
                                        save_to_filepath="trend_analysis.png"
                                        )

    except Exception as e:
        print(f"An error occurred in main: {e}")
