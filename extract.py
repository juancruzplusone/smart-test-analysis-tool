import pandas as pd
import re

class SMART_Table:
    """
    Class to extract and interpret SMART data from a log file.
    """
    def __init__(self, smart_log_file_path):
        self.smart_log_file_path = smart_log_file_path
        self.raw_data_table = None
        self.filtered_data_table = None

        self._extract_smart_table()

        self.attribute_map = {
                '01': 'Raw Read Error Rate',
                '09': 'Power On Hours',
                '0C': 'Power Cycle Count',
                '0E': 'Device Capacity',
                '0F': 'User Capacity',
                '10': 'Initial Spare Blocks Available',
                '11': 'Spare Blocks Remaining',
                '64': 'Total Erase Count',
                'A8': 'SATA PHY Error Count',
                'AA': 'Bad Block Count',
                'AD': 'Erase Count',
                'AE': 'Unexpected Power Loss Count',
                'AF': 'Power Failure Protection Status',
                'C0': 'Unexpected Power Loss Count',
                'C2': 'Temperature',
                'CA': 'Percentage of Spares Remaining',
                'DA': 'CRC Error',
                'E7': 'SSD Life Remaining',
                'EA': 'Total NAND Read',
                'EB': 'Total NAND Written',
                'F1': 'Total Host Write',
                'F2': 'Total Host Read'
            }

    def interpret_table(self) -> pd.DataFrame:
        """
         Filters and updates the table attributes and ID based on Advantech Whitepaper:
           https://advantechfiles.blob.core.windows.net/cms/fcbe7247-8637-4586-ba0a-c74ec4bb4e07/Whitepaper%20%20PDF%20File/Whitepaper5_Intelligent-Self-Management-Technology_0327.pdf
        """
        # Convert ID# to hexadecimal and map to new attribute names
        self.raw_data_table['ID#'] = self.raw_data_table['ID#'].apply(lambda x: format(int(x), '02X'))
        self.raw_data_table['ATTRIBUTE_NAME'] = self.raw_data_table['ID#'].map(self.attribute_map).fillna(self.raw_data_table['ATTRIBUTE_NAME'])

        # Retain only the specified columns
        self.filtered_data_table = self.raw_data_table[['ID#', 'ATTRIBUTE_NAME', 'RAW_VALUE']]

        return self.filtered_data_table

    def generate_new_report(self, new_file_path: str):
        """
        Generates a new report with the filtered data.
        """
        self.filtered_data_table.to_csv(new_file_path, index=False)


    def _extract_smart_table(self):
        try:
            with open(self.smart_log_file_path, 'r') as file:
                lines = file.readlines()

            start_idx = next(i for i, line in enumerate(lines) if "Vendor Specific SMART Attributes with Thresholds:" in line) + 2
            end_idx = next(i for i, line in enumerate(lines) if "SMART Error Log Version: 1" in line)

            table_lines = lines[start_idx:end_idx]
            columns = ["ID#", "ATTRIBUTE_NAME", "FLAG", "VALUE", "WORST", "THRESH", "TYPE", "UPDATED", "WHEN_FAILED", "RAW_VALUE"]

            data = []
            for line in table_lines:
                parsed_line = re.split(r'\s+', line.strip())
                # Handling the special case where 'RAW_VALUE' includes 'Min/Max'
                if len(parsed_line) > len(columns):
                    raw_index = 8  # Assuming 'RAW_VALUE' is the last column before it potentially gets incorrectly split
                    parsed_line = parsed_line[:raw_index] + [" ".join(parsed_line[raw_index:])]
                if len(parsed_line) == len(columns):
                    data.append(parsed_line)

            self.raw_data_table = pd.DataFrame(data, columns=columns)
        except Exception as e:
            print(f"An error occurred: {e}")



class SMART_Analyzer:
    """
    Analyzes the SMART data from two tests.
    """
    def __init__(self, old_test: SMART_Table, new_test: SMART_Table, 
                    testing_time: float, ssd_flash_code: str):
        self.old_test = old_test
        self.new_test = new_test
        self.testing_time = testing_time
        self.ssd_flash_code = ssd_flash_code

        self.flash_code_attributes = {
                'S': 60000,
                'U': 30000,
                'M': 3000,
                'V': 3000,
                'C': 5000,
                'Z': 50000
            }
        
        self.max_PE_cycles = self.flash_code_attributes[self.ssd_flash_code]

    def calculate_usable_life(self) -> float:
        pass


if __name__ == "__main__":
    file_path = "some_file_path_here"
    test_00 = SMART_Table(file_path)
    interpreted_data = test_00.interpret_table()

    new_file_path = "some_new_file_path_here"
    test_00.generate_new_report(new_file_path)
